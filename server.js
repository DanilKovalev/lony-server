var express = require('express');
var URL = require('url');
var bodyParser = require('body-parser');
var app = express();
var MongoClient = require('mongodb').MongoClient;
var port = 27018;
var ObjectID = require('mongodb').ObjectID;
var db;

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

function guid() {
    function s4() {
        return Math.floor((1 + Math.random()) * 0x10000)
            .toString(16)
            .substring(1);
        }
        return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
        s4() + '-' + s4() + s4() + s4();
}

function successWith(data) {
    return {"status": "OK", "data": data}
}

function errorWith(data) {
    return {"status": "ERROR", "data": data}
}

app.post('/register', function(req, res) {
    let user = {
        "phoneNumber": req.body.phoneNumber,
        "password": req.body.password,
        "registerDate": req.body.registerDate,
        "languageId": req.body.languageId,
        "name": req.body.name,
        "birthdayDate": req.body.birthdayDate,
        "sex": req.body.sex,
        "city": req.body.country,
        "lng": req.body.lng,
        "ltd": req.body.ltd,
        "interests": req.body.interests,
        "avatarUrl": req.body.avatarUrl
    }

    db.collection('users').insert(user, function(err, result) {
        if (err) {
            return res.send(errorWith(err));
        } else {
            return res.send(successWith(result));
        }
    })
});

app.post('/login', function(req, res) {
    let useres = db.collection('users').find({
        phoneNumber: req.body.phoneNumber,
        password: req.body.password
    }).toArray(function(err, docs) {
        if (err) {
            return res.send(errorWith(err));
        } else {
            if (docs.length == 1) {
                return res.send(successWith(docs));
            } else {
                return res.send(errorWith("Database have no users with this parameters or more users than one."));
            }
            
        }
    })
})

app.get('/users', function(req, res) {
    let useres = db.collection('users').find().toArray(function(err, docs) {
        if (err) {
            return res.send(errorWith(err));
        } else {
            return res.send(successWith(docs));
        }
    })
})

app.get('/dropDB', function(req,res) {
    db.dropDatabase();
    res.sendStatus(200);
});


MongoClient.connect('mongodb://127.0.0.1', function(err, database) {
    if (err) {
        return console.log(err);
    }

    db = database;
    app.listen(port, function() {
        console.log('Server start listenings' + ';\n');
    })
})